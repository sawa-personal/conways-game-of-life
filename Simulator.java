//*******************************************************
// Simulator.java
// created by Sawada Tatsuki on 2017/12/05.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* シミュレータ本体のクラス */

import java.util.*;

public class Simulator extends AbstractSimulator {
	public int WIDTH = 80;
	public int HEIGHT = 80;
    public boolean curCell[][];
    public boolean nextCell[][];
    private int dr[] = {0, 1, 1, 1, 0, -1, -1, -1, 0, 1}; //8近傍探索用
    private int neighbortNum;
    
    public Simulator() {
		curCell = new boolean[HEIGHT+2][WIDTH+2];
		nextCell = new boolean[HEIGHT+2][WIDTH+2];
		neighbortNum = dr.length - dr.length / 5;
		//壁の作成
		for(int i = 0; i < WIDTH + 2; i++) {
			curCell[0][i] = false;
			curCell[WIDTH + 1][i] = false;
			nextCell[0][i] = false;
			nextCell[WIDTH + 1][i] = false;
		}
		for(int i = 1; i < HEIGHT + 1; i++) {
			curCell[i][0] = false;
			curCell[i][HEIGHT + 1] = false;
			nextCell[i][0] = false;
			nextCell[i][HEIGHT + 1] = false;
		}

		//生か死かランダム
		for(int i = 1; i < HEIGHT + 1; i++) {
			for(int j = 1; j < WIDTH + 1; j++) {
				if(rnd.nextInt(2) == 0) {
					curCell[i][j] = true;
				} else {
					curCell[i][j] = false;
				}
			}
		}
    }
    
    public void process() {
		for(int i = 1; i < HEIGHT + 1; i++) {
			for(int j = 1; j < WIDTH + 1; j++) {
				boolean cell = curCell[i][j]; //着目セル
				int count = 0; //カウンタ
				//着目セルの8近傍(ムーア近傍)に生きているセルを数える
				for(int r = 0; r < 8; r++) {
					if(curCell[i + dr[r+2]][j + dr[r]]) {
						count++;
					}
				}
		
				if(cell) { //着目セルが生きていた場合
					nextCell[i][j] = (count == 2 || count == 3) ? true : false;
				} else { //着目セルが死んでいた場合
					nextCell[i][j] = (count == 3) ? true : false;
				}
			}
		}
	
		//現在のセルを更新
		for(int i = 1; i < HEIGHT + 1; i++) {
			for(int j = 1; j < WIDTH + 1; j++) {
				curCell[i][j] = nextCell[i][j];
			}
		}
    }
}
