//*******************************************************
// Main.java
// created by Sawada Tatsuki on 2017/11/12.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* グリッドをアニメーションさせるクラス */

import java.util.*;

public class Main extends Grid2D {
    private int width; //横のセル数
	private int height; //縦のセル数

    private boolean life[][];
    
    public void setting() {
		title("Conways Game of Life"); //タイトル
		width = simulator.WIDTH; //横のセル数を取得
		height = simulator.HEIGHT; //縦セル数を取得
		size(width, height);
		refRate(80);
		cellSize(4, 4);
    }
    
    public void setup() {
		life = getCell();
		for(int i = 0; i < height; i++){
			for(int j = 0; j < width; j++){
				String color = life[i][j] ? "#ffffff" : "#000000";
				cell(j, i, color);
			}
        }
    }
    
    public void loop() {
		life = getCell();
		for(int i = 0; i < height; i++){
			for(int j = 0; j < width; j++){
				String color = (life[i][j] == true) ? "#ffffff" : "#000000";
				cell(j, i, color);
			}
        }
    }

	public boolean[][] getCell() {
		boolean cell[][] = new boolean[height][width];
		for(int i = 0; i < height; i++) {
			for(int j = 0; j < width; j++) {
				cell[i][j] = simulator.curCell[i+1][j+1];
			}
		}
		return cell;
    }
	
    public static void main(String[] args) {
		launch(args); //Gridを描画する
    }    
}
