//*******************************************************
// Grid2D.java
// created by Sawada Tatsuki on 2017/12/18.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 二次元格子のクラス */

//JavaFXで描画処理を繰り返すときに用いる
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.util.Duration;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public abstract class Grid2D extends Application {
	protected Simulator simulator; //シミュレータのインスタンス
    private GridPane gridPane; //グリッド
    private Label[][] label; //グリッドのセル
    
    private int windowWidth = 16; //横のセル数
    private int windowHeight = 16; //縦のセル数

	public Grid2D() {
		simulator = new Simulator(); //シミュレータを生成
	}
	
    public abstract void setting(); //オーバライド前提
    public abstract void setup(); //オーバライド前提
    public abstract void loop(); //オーバライド前提
	
    //縦横のセル数の設定
    protected void size(int width, int height){
		this.windowWidth = width;
		this.windowHeight = height;
    }

    private int cellWidth = 64; //セルの横幅
    private int cellHeight = 64; //セルの縦幅
    //セルの大きさを設定
    protected void cellSize(int width, int height) {
		cellWidth = width;
		cellHeight = height;
    }
    
    private int refRate = 10;
    //描画のリフレッシュレートを設定
    protected void refRate(int t) {
		refRate = t;
    }    
    
    private String windowTitle = ""; //タイトルバーの文字
    //タイトルバーの文字の設定
    protected void title(String title) {
		windowTitle = title;
    }
    
    //cellの色を変更
    protected void cell(int w, int h, String color){
		label[h][w].setStyle("-fx-background-color: " + color + ";");
    }
    
    //ステージを描画
    @Override public void start(Stage stage) throws Exception {
		setting(); //セッティング処理:オーバーライド前提
		createGrid(); //グリッドを作成
		setup(); //セットアップ処理:オーバーライド前提

		Scene scene = new Scene(gridPane); //gridPaneをsceneに追加する
		stage.setScene(scene); //stageにsceneをセット
		stage.setTitle(windowTitle); //タイトルバーの文字を設定
		stage.show();// stageの描画

        Timeline timer = new Timeline(new KeyFrame(Duration.millis(refRate),new EventHandler<ActionEvent>(){
				@Override public void handle(ActionEvent event) {
					simulator.run(); //系を動かす
					loop(); //ループ処理:オーバーライド前提
				}
			}));
		timer.setCycleCount(Timeline.INDEFINITE);
		timer.play();
    }

    private void createGrid() {
		gridPane = new GridPane(); //GridPaneの生成
        label = new Label[windowHeight][windowWidth]; //セルの生成
		for(int i = 0; i < windowHeight; i++){
			for(int j = 0; j < windowWidth; j++){
				label[i][j] = new Label();
				label[i][j].setMinSize(1, 1); //セルの最小サイズ
				label[i][j].setPrefSize(cellWidth, cellHeight); //セルのサイズを指定
				label[i][j].setStyle("-fx-padding: 0;-fx-margin: 0;");
				gridPane.add(label[i][j], j, i); //GridPaneに追加
			}
		}
    }
}
