//*******************************************************
// AbstractSimmulator.java
// created by Sawada Tatsuki on 2017/12/25.
// Copyright © 2017 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* シミュレータの抽象クラス */

import java.util.*;

public abstract class AbstractSimulator {
	private int t; //経過ステップ数
	
	private int dr4[] = {0, 1, 0, -1, 0}; //4近傍(ノイマン近傍)
    private int dr8[] = {0, 1, 1, 1, 0, -1, -1, -1, 0, 1}; //8近傍(ムーア近傍)

	protected Random rnd = new Random(); //乱数生成用インスタンス
	
    //コンストラクタ
    public AbstractSimulator() {
		t = 0; //ステップ数を初期化		
    }
    
    ///世界を単位時間だけ動かす
    public void run(){
		process(); //処理を実行
		t++; //ステップ数をカウント
    }

    abstract protected void process();	//シミュレータの処理内容: オーバーライド前提
	
	//現在のステップを取得
	public int getStep() {
		return t;
	}

}
